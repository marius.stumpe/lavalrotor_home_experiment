"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    absolute_value = np.sqrt(np.square(x) + np.square(y) + np.square(z))
    return(absolute_value)


def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    interpolation_timestamp = np.linspace(min(time),max(time),1000)
    interpolated_acceleration = np.interp(interpolation_timestamp, time, data)
    return([interpolation_timestamp, interpolated_acceleration])


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    [interpolation_timestamp, interpolated_acceleration] = interpolation(time, x)
    
    acceleration_for_fft = interpolated_acceleration - np.mean(interpolated_acceleration)
    
    fft = np.fft.fft(acceleration_for_fft)
    amp = np.abs(fft)
    freq = np.fft.fftfreq(len(amp))
    return(amp[1:500], freq[1:500])